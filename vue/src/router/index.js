import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
    {
        path: "/",
        name: "layout",
        component: () => import("@/layouts/MainLayout.vue"),        
        children: [
            {
                path: "",
                name: "home",
                component: () => import("@/pages/Home.vue"),
                meta: {
                    title: 'Интернет-магазин вебинары для педагогов',
                    metaTags: [
                      {
                        name: 'description',
                        content: 'Вебинары для школы и дошкольных учреждений'
                      },
                      {
                        property: 'description',
                        content: 'Здесь можно приобрести вебинары и обучающие материалы'
                      }
                    ]
                  },
                props: true
            },
            {
                path: "ware/:id",
                name: "ware",
                component: () => import("@/pages/Ware.vue"),
            },            
            {
                path: "pages/:slug",
                name: "pages",
                component: () => import("@/pages/Page.vue"),
            },
            {
                path: "edu_program/",
                name: "eduProgram",
                component: () => import("@/pages/EduProgram.vue"),
            },           
        ],
    },
    {
        path: "/user",
        name: "user",
        component: () => import("@/layouts/NavigationOnly.vue"),
        children: [
            {
                path: "/login",
                name: "login",
                component: () => import("@/pages/Home.vue"),
            },            
            {
                path: "/profile",
                name: "profile",
                component: () => import("@/pages/user/Profile.vue"),
            },            
            {
                path: "/log",
                name: "log",
                component: () => import("@/pages/user/Log.vue"),
            },
        ],
    }    
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes,
});

export default router;

router.beforeEach((to, from, next) => {
    // redirect to login page if not logged in and trying to access a restricted page
    const personalPages = ["/profile/", "/profile", "/log", "/log/"];
    const authRequired = personalPages.includes(to.path);
    const loggedIn = localStorage.getItem("user");

    if (authRequired && !loggedIn) {
        return next("/login");
    }

    next();
});
