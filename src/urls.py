"""webschoolru URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path
from django.contrib.auth.models import User
from src.main.views import front_render_view


urlpatterns = [
    path('', front_render_view),
    path('api/', include('src.main.urls')),
    path('user/', include('src.users.urls')),
    # admin
    path('admin/', admin.site.urls, name='admin'),
    # ckeditor
    path('ckeditor/', include('ckeditor_uploader.urls')),
    # re_path('^.*$', front_render_view), # Для ответа на любой URL по прямой ссылке 
]

handler404 = 'src.main.views.front_render_view'
