from django.contrib import admin
from .models import *
from django.shortcuts import redirect
from src.main.models import *
from django.contrib.contenttypes.models import ContentType

class WarePaid(admin.TabularInline):
    model = Log
    verbose_name = "оплаченный товар"
    verbose_name_plural = "оплаченные товары"
    def get_queryset(self, request):
        qs = super(WarePaid, self).get_queryset(request)
        return qs.filter(paid_dt__isnull=False, content_type=ContentType.objects.get_for_model(Ware))
    extra = 0

class AuthCertPaid(admin.TabularInline):
    model = Log
    verbose_name = "оплаченное удостоверение"
    verbose_name_plural = "оплаченные удостоверения"
    def get_queryset(self, request):
        qs = super(AuthCertPaid, self).get_queryset(request)
        return qs.filter(paid_dt__isnull=False, content_type=ContentType.objects.get_for_model(AuthCert))
    extra = 0


class UserAdmin(admin.ModelAdmin):
    change_form_template = "admin/user/form.html"
    def response_change(self, request, obj):
        if "login" in request.POST:
            obj.save()
            temp_link = TempLink.objects.create(user=obj)
            return redirect(f"https://web-pedagogi.ru?link={temp_link.value}")            
        return super().response_change(request, obj)
    inlines = [WarePaid, AuthCertPaid]


admin.site.register(User, UserAdmin)

#admin.site.register(TempLink)