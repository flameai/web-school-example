from .views import CreateUserAPIView
from django.urls import path, include
from src.users import views as user_views
from rest_framework_jwt.views import obtain_jwt_token
from rest_framework_jwt.views import refresh_jwt_token
from rest_framework_jwt.views import verify_jwt_token



 
urlpatterns = [
    path('create/', CreateUserAPIView.as_view()),
    path('obtain_token/', user_views.authenticate_user), # логин через форму
    path('obtain_token_with_link/', user_views.authenticate_user_with_link), # запрос на токен по ссылке из почты
    path('<int:id>/update/', user_views.UserUpdateAPIView.as_view()), # обновление профиля     
    path('recover/', user_views.RestoreUserAPIView.as_view()), # запрос на восстановление 
    path('recover_or_create/', user_views.RestoreOrCreateUserAPIView.as_view()), # запрос на восстановление или создание
]