# users/serializers.py
from rest_framework import serializers
from.models import User


class UserSerializer(serializers.ModelSerializer):

    date_joined = serializers.ReadOnlyField()

    class Meta(object):
        model = User
        fields = ('id', 'first_name', 'last_name', 'middle_name', 'position', 'city', 'tel',
                  'date_joined', 'password', 'organization', 'email', 'post_address')        
        extra_kwargs = {'password': {'write_only': True}}


class UserUpdateSerializer(serializers.ModelSerializer):

    class Meta(object):
        model = User
        fields = ('first_name', 'last_name', 'middle_name', 'position',
                  'city', 'tel', 'organization', 'post_address', 'password')
        extra_kwargs = {'password': {'write_only': True}}