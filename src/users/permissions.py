from rest_framework.permissions import BasePermission

class IsMyAccount(BasePermission):
    def has_permission(self, request, view):
        return view.kwargs['id'] == request.user.id