from django.db import models
from django.utils import timezone
from django.db import transaction
import datetime
from django.conf import settings
import string
import random
from django.contrib.auth.models import (
    AbstractBaseUser, PermissionsMixin, BaseUserManager
)

class UserManager(BaseUserManager):
 
    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email,and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        try:
            with transaction.atomic():
                user = self.model(email=email, **extra_fields)
                user.set_password(password)
                user.save(using=self._db)
                return user
        except:
            raise
 
    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)
 
    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
 
        return self._create_user(email, password=password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.
 
    """
    email = models.EmailField(max_length=40, unique=True, blank=False, null=False, verbose_name="Электронная почта")
    first_name = models.CharField(max_length=50, blank=True, verbose_name="Имя")
    last_name = models.CharField(max_length=50, blank=True, verbose_name="Фамилия")
    middle_name = models.CharField(max_length=50, blank=True, verbose_name="Отчество")
    position = models.CharField(max_length=70, blank=True, verbose_name="Должность")
    organization = models.CharField(max_length=70, blank=True, verbose_name="Организация")
    city = models.CharField(max_length=70, blank=True, verbose_name="Город")
    tel = models.CharField(max_length=70, blank=True, verbose_name="Телефон")
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    date_joined = models.DateTimeField(auto_now_add=True)
    last_login_dt = models.DateTimeField(null=True, blank=True)
    description = models.TextField(verbose_name="Внутреннее описание пользователя", default='', blank=True)    
    post_address = models.CharField(max_length=200, blank=True, verbose_name="Почтовый адрес")

    objects = UserManager()
 
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
 
    def save(self, *args, **kwargs):
        super(User, self).save(*args, **kwargs)
        return self
    
    class Meta:
        verbose_name = "пользователь"
        verbose_name_plural = "пользователи"
    
    def __str__(self):
        return f"{self.email}"



class TempLink(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    exp_date_time = models.DateTimeField(verbose_name="Дата и время, когда ссылка истечет", blank=True, null=True)
    value = models.CharField(verbose_name="Значение ссылки", blank=True, null=True, max_length=100)

    class Meta:        
        verbose_name = 'одноразовая ссылка для входа'
        verbose_name_plural = 'одноразовые ссылки для входа'
    
    def __str__(self):
        return f"{self.user.email}"

    def save(self, *args, **kwargs):
        if not self.pk:
            self.exp_date_time = timezone.now() + datetime.timedelta(seconds=settings.LINKS_EXP_TIME)
            # Создадим случайную ссылку
            temp_value = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(50))
            while TempLink.objects.filter(value=temp_value).exists():
                temp_value = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(50))
            self.value = temp_value
        super(TempLink, self).save(*args,**kwargs)
