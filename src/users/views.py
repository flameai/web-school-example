from django.shortcuts import render
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAuthenticated 
from .permissions import IsMyAccount
from rest_framework.decorators import api_view, permission_classes
from .models import *
import jwt
from django.conf import settings
from rest_framework_jwt.utils import jwt_payload_handler
from .serializers import UserSerializer, UserUpdateSerializer
from src.main.views import Mailer
from django.utils import timezone
from src.main.views import Mailer
from rest_framework_jwt.authentication import JSONWebTokenAuthentication


class CreateUserAPIView(APIView):
    # Allow any user (authenticated or not) to access this url
    permission_classes = (AllowAny,)

    def post(self, request):
        user = request.data
        serializer = UserSerializer(data=user)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class UserUpdateAPIView(APIView):
    """
    Апи изменения данных пользователя /user/<id>/update
    """
    # Allow only authenticated users to access this url
    permission_classes = [IsMyAccount,]
    serializer_class = UserUpdateSerializer
    authentication_classes = [JSONWebTokenAuthentication,]

    def patch(self, request, *args, **kwargs):
        serializer_data = request.data

        serializer = self.serializer_class(
            request.user, data=serializer_data, partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_200_OK)

# obtain_token
@api_view(['POST'])
@permission_classes([AllowAny, ])
def authenticate_user(request):

    try:
        email = request.data['email'].lower()
        password = request.data['password']
        user = User.objects.get(email=email, password=password)
        if user:
            try:
                payload = jwt_payload_handler(user)
                token = jwt.encode(payload, settings.SECRET_KEY)
                user_details = {}
                user_details = UserSerializer(user).data
                user_details['token'] = token
                # Пока не будем записывать время последнего логина
                # user_logged_in.send(sender=user.__class__,
                #                     request=request, user=user)
                return Response(user_details, status=status.HTTP_200_OK)

            except Exception as e:
                raise e
        else:
            res = {
                'error': 'can not authenticate with the given credentials or the account has been deactivated'}
            return Response(res, status=status.HTTP_403_FORBIDDEN)
    except:
        res = {'error': 'Неправильное имя пользователя или пароль'}
        return Response(res, status=status.HTTP_403_FORBIDDEN)

# obtain_token_by_link
@api_view(['POST'])
@permission_classes([AllowAny, ])
def authenticate_user_with_link(request):

    try:
        link = request.data['link']
        temp_link = TempLink.objects.get(value=link, exp_date_time__gt=timezone.now())
        user = temp_link.user
        if user:
            try:
                payload = jwt_payload_handler(user)
                token = jwt.encode(payload, settings.SECRET_KEY)
                user_details = {}
                user_details = UserSerializer(user).data
                user_details['token'] = token
                # Пока не будем записывать время последнего логина
                # user_logged_in.send(sender=user.__class__,
                #                     request=request, user=user)
                temp_link.delete()
                return Response(user_details, status=status.HTTP_200_OK)

            except Exception as e:
                raise e
        else:
            res = {
                'error': 'Ссылка истекла или недействительна'}
            return Response(res, status=status.HTTP_403_FORBIDDEN)
    except:
        res = {'error': 'Ссылка истекла или недействительна'}
        return Response(res, status=status.HTTP_403_FORBIDDEN)


class RestoreOrCreateUserAPIView(APIView, Mailer):
    permission_classes = (AllowAny,)

    def post(self, request):
        # Пришел запрос либо на восстановление пароля либо на создание нового пользователя
        if not 'email' in request.data:
            return Response({"status":"ok"}, status.HTTP_200_OK) # Не будем говорить здесь, что не нашли
        email = request.data['email'].lower()
        recipient, created = User.objects.get_or_create(email=email)
        redirect = request.data['redirect'] if 'redirect' in request.data else None
        if created:
            Mailer.send_create_mail(recipient=recipient, request=request, redirect=redirect)
        else:
            Mailer.send_restore_mail(recipient=recipient, request=request, redirect=redirect)
        return Response({"status":"ok"}, status.HTTP_200_OK)

class RestoreUserAPIView(APIView, Mailer):
    permission_classes = (AllowAny,)

    def post(self, request):
        # Пришел запрос либо на восстановление пароля либо на создание нового пользователя
        if not 'email' in request.data:
            return Response({"status":"ok"}, status.HTTP_200_OK) # Не будем говорить здесь, что не нашли
        email = request.data['email'].lower()        
        recipient = User.objects.filter(email=email).first()
        redirect = request.data['redirect'] if 'redirect' in request.data else None
        if recipient:
            Mailer.send_restore_mail(recipient=recipient, request=request, redirect=redirect)
        # В любом случае 200
        return Response({"status":"ok"}, status.HTTP_200_OK)