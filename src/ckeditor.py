CKEDITOR_CONFIGS = {
    'main_ckeditor': {
        'toolbar': 'Custom',
        'toolbar_Custom': [
            ['Bold', 'Italic', 'Underline'],
            ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-',
                'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
            ['Link', 'Unlink', 'CreateDiv'],
            ['RemoveFormat', 'Source'],
            ['TextColor', 'BGColor']
        ],
        'stylesSet': [
            {
                'name': 'Серый',
                'element': 'div',
                'styles': {
                    'padding': '5px 10px',
                    'background': '#eee',
                    'border': '1px solid #ccc',
                    'border-radius': '5px',
                    'box-shadow': '0 0 10px rgba(0,0,0,0.5)'
                }
            },
            {
                'name': 'Голубой',
                'element': 'div',
                'styles': {
                    'padding': '5px 10px',
                    'background': '#b1d4ff',
                    'border': '1px solid #ccc',
                    'border-radius': '5px',
                    'box-shadow': '0 0 10px rgba(0,0,0,0.5)'
                }
            },
            {
                'name': 'Зеленый',
                'element': 'div',
                'styles': {
                    'padding': '5px 10px',
                    'background': '#b4ffb4',
                    'border': '1px solid #ccc',
                    'border-radius': '5px',
                    'box-shadow': '0 0 10px rgba(0,0,0,0.5)'
                }
            }

        ]
    },
    'page': {
        'toolbar': 'Custom',
        'toolbar_Custom': [["Format", "Bold", "Italic", "Underline", "Strike", "SpellChecker"],
                           ['NumberedList', 'BulletedList', "Indent", "Outdent", 'JustifyLeft', 'JustifyCenter',
                            'JustifyRight', 'JustifyBlock'],
                           ["Image", "Table", "Link", "Unlink", "Anchor", "SectionLink",
                               "Subscript", "Superscript"], ['Undo', 'Redo'], ["Source"],
                           ["Maximize"]],
        ['TextColor', 'BGColor']
        'stylesSet': [
            {
                'name': 'Серый',
                'element': 'div',
                'styles': {
                    'padding': '5px 10px',
                    'background': '#eee',
                    'border': '1px solid #ccc',
                    'border-radius': '5px',
                    'box-shadow': '0 0 10px rgba(0,0,0,0.5)'
                }
            },
            {
                'name': 'Голубой',
                'element': 'div',
                'styles': {
                    'padding': '5px 10px',
                    'background': '#b1d4ff',
                    'border': '1px solid #ccc',
                    'border-radius': '5px',
                    'box-shadow': '0 0 10px rgba(0,0,0,0.5)'
                }
            },
            {
                'name': 'Зеленый',
                'element': 'div',
                'styles': {
                    'padding': '5px 10px',
                    'background': '#b4ffb4',
                    'border': '1px solid #ccc',
                    'border-radius': '5px',
                    'box-shadow': '0 0 10px rgba(0,0,0,0.5)'
                }
            }

        ]
    },
    'default': {
        'toolbar': 'Custom',
        'toolbar_Custom': [
            ['Bold', 'Italic', 'Underline'],
            ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-',
                'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
            ['Link', 'Unlink', 'CreateDiv'],
            ['RemoveFormat', 'Source'],
        ],
        'stylesSet': [
            {
                'name': 'Серый',
                'element': 'div',
                'styles': {
                    'padding': '5px 10px',
                    'background': '#eee',
                    'border': '1px solid #ccc',
                    'border-radius': '5px',
                    'box-shadow': '0 0 10px rgba(0,0,0,0.5)'
                }
            },
            {
                'name': 'Голубой',
                'element': 'div',
                'styles': {
                    'padding': '5px 10px',
                    'background': '#b1d4ff',
                    'border': '1px solid #ccc',
                    'border-radius': '5px',
                    'box-shadow': '0 0 10px rgba(0,0,0,0.5)'
                }
            },
            {
                'name': 'Зеленый',
                'element': 'div',
                'styles': {
                    'padding': '5px 10px',
                    'background': '#b4ffb4',
                    'border': '1px solid #ccc',
                    'border-radius': '5px',
                    'box-shadow': '0 0 10px rgba(0,0,0,0.5)'
                }
            }

        ]
    }, }
