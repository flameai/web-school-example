from django.db import models

EDCL_GREEN = "#b4ffb4"
EDCL_BLUE = "#b1d4ff"
EDCL_GRAY = "#eee"
EDCL_PINK = "#ffbb96"
EDCL_YELLOW = "#ffee96"

EDCL_CHOICE = (
    (EDCL_GREEN, "Зеленый"),
    (EDCL_BLUE, "Голубой"),
    (EDCL_GRAY, "Серый"),
    (EDCL_PINK, "Розовый"),
    (EDCL_YELLOW, "Желтый"),
)

class EduProgram(models.Model):
    name = models.CharField(verbose_name="Название",
                            default='', blank=True, null=True, max_length=1024)
    color = models.CharField(choices=EDCL_CHOICE, max_length=7, default=EDCL_GRAY, null=False)
    upload = models.FileField(upload_to='uploads/', verbose_name="Файл с описанием", null=True, blank=True)
    hours = models.PositiveIntegerField(verbose_name="количество часов", null=True)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        verbose_name = 'образовательная программа'
        verbose_name_plural = 'образовательные программы'
