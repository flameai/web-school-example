from django.db import models
from sort_order_field import SortOrderField

class Project(models.Model):
    name = models.CharField(verbose_name="Название",
                            max_length=512, blank=True)
    link = models.CharField(verbose_name="Ссылка", max_length=512, blank=True)
    image = models.ImageField(
        verbose_name="Файл изображения", blank=True, null=True, upload_to='projects')
    sort_order = SortOrderField(("Порядок"))
    hidden = models.BooleanField(verbose_name="Скрыть", default=True)

    class Meta:
        verbose_name = 'проект'
        verbose_name_plural = 'проекты'