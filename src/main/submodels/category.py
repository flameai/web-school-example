from django.db import models

class Category(models.Model):
    name = models.CharField(verbose_name="Название",
                            default='', blank=True, null=True, max_length=1024)
    hidden = models.BooleanField(verbose_name="Скрыть", default=True)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        verbose_name = 'категория'
        verbose_name_plural = 'категории'
