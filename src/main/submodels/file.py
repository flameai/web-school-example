from django.db import models
from .ware import *

class File(models.Model):
    name = models.CharField(verbose_name="Название файла", max_length=512)
    created_date = models.DateTimeField(
        auto_now_add=True, verbose_name="Дата создания файла")
    data = models.FileField(verbose_name="Файл", blank=True, null=True)
    ware = models.ForeignKey(
        Ware, null=True, on_delete=models.CASCADE, blank=True)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        verbose_name = 'файлы'
        verbose_name_plural = 'файлы'