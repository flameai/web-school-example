from django.db import models
from ckeditor.fields import RichTextField


class Adjust(models.Model):
    slug = models.SlugField(verbose_name="код настройки",
                            null=False, blank=False)
    name = models.CharField(
        max_length=100, verbose_name='название', null=True, blank=True)
    text = RichTextField(
        verbose_name="Текст", max_length=20480)

    class Meta:
        verbose_name = "настройка сайта"
        verbose_name_plural = "настройки сайта"

    def __str__(self):
        return f"{self.name}"
