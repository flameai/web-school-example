from django.db import models
from django.utils import translation
from PIL import Image, ImageFont, ImageDraw
from django.utils.formats import date_format
from datetime import datetime
import textwrap


class Cert(models.Model):
    name = models.CharField(verbose_name="Название",
                            default='', blank=True, null=True, max_length=128)
    image = models.ImageField(
        verbose_name="Файл изображения сертификата", blank=True, null=True, upload_to='certs')
    x1 = models.PositiveIntegerField(
        verbose_name="x для ФИО", default=0, blank=True)
    y1 = models.PositiveIntegerField(
        verbose_name="y для ФИО", default=0, blank=True)
    arrange_fio = models.BooleanField(
        verbose_name="выровнять ФИО посередине сертификата", default=True)
    size_fio = models.PositiveIntegerField(
        verbose_name="размер текста для ФИО", default=10, blank=True)
    x2 = models.PositiveIntegerField(
        verbose_name="x для Даты", default=0, blank=True)
    y2 = models.PositiveIntegerField(
        verbose_name="y для Даты", default=0, blank=True)
    size_date = models.PositiveIntegerField(
        verbose_name="размер текста для даты", default=10, blank=True)
    x3 = models.PositiveIntegerField(
        verbose_name="x для Названия", default=0, blank=True)
    y3 = models.PositiveIntegerField(
        verbose_name="y для Названия", default=0, blank=True)
    arrange_title = models.BooleanField(
        verbose_name="выровнять название курса посередине сертификата", default=True)
    title_max_line_size = models.PositiveIntegerField(
        verbose_name="максимальная длина строки курса в символах", default=90, blank=True)
    size_title = models.PositiveIntegerField(
        verbose_name="размер текста для названия курса", default=10, blank=True)

    def __str__(self):
        return f"{self.name}"

    def insert_text(self, text_fio=None, text_date=None, text_title=None, result_file=""):
        translation.activate('ru')
        font_fio = ImageFont.truetype("fonts/uni_bold.ttf", self.size_fio)
        text_fio = text_fio if text_fio else "Константин Константинович Константиновский"
        font_date = ImageFont.truetype("fonts/uni.ttf", self.size_date)
        text_date = text_date if text_date else date_format(datetime.today())
        font_title = ImageFont.truetype("fonts/uni.ttf", self.size_title)
        text_title = text_title if text_title else "Вебинар \"Игры и игровые упражнения как комплексное решение задачи по коррекции звукопроизношения и формированию пространственных координаций. Эффективное средство профилактики дислексии и дисграфии у детей с тяжелыми нарушениями речи"
        img = Image.open(self.image.file)
        draw = ImageDraw.Draw(img)
        W = img.width
        w_fio, h_fio = ImageDraw.Draw(img).textsize(text_fio, font=font_fio)
        # FIO
        x_title = (W-w_fio)/2 if self.arrange_fio else self.x1
        draw.text((x_title, self.y1), text_fio, (0, 0, 0), font=font_fio)
        # DATE
        draw.text((self.x2, self.y2), text_date, (0, 0, 0), font=font_date)
        # TITLE
        lines = textwrap.wrap(text_title, width=self.title_max_line_size)
        y_title = self.y3
        for line in lines:
            line_width, line_height = font_title.getsize(line)
            x_title = (W - line_width) / 2 if self.arrange_title else self.x3
            draw.text((x_title, y_title), line,
                      font=font_title, fill=(0, 0, 0))
            y_title += line_height
        img.save(result_file if result_file != '' else 'temp_cert.jpg')
        return img

    class Meta:
        verbose_name = 'сертификат'
        verbose_name_plural = 'сертификаты'
