
from django.db import models
from src.main.models import *
from django.contrib.contenttypes.fields import GenericRelation
from django.conf import settings
from src.main.submodels.log import *



class AuthCert(models.Model):
    name = models.CharField(verbose_name="Название",
                            default='', blank=True, null=True, max_length=1024)
    edu_program = models.ForeignKey('EduProgram', verbose_name="Образовательная программа",
                                    null=True, on_delete=models.DO_NOTHING, blank=True, related_name="auth_certs")     
    hours = models.PositiveSmallIntegerField(
        verbose_name="количество часов, набранных по образовательной программе, для получения", null=True, blank=True)
    logs = GenericRelation('Log', related_query_name='auth_cert')
    price = models.IntegerField(
        verbose_name="Стоимость услуги посылки", null=True, blank=True)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        verbose_name = 'удостоверение'
        verbose_name_plural = 'удостоверения'
    
    def buy(self, user):
        if not self.pk:
            return None
        log_order = Log.objects.create(user=user, content_type=ContentType.objects.get(app_label='main', model='ware'), type=LOG_TYPE_ORDER, object_id=self.pk, price=self.price)        
        log_order.save()
        log_payment = Log.objects.create(user=user, content_type=ContentType.objects.get(app_label='main', model='ware'), type=LOG_TYPE_PAYMENT, object_id=self.pk, price=self.price)        
        log_payment.save()