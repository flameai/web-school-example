from django.db import models
from ckeditor.fields import RichTextField


class Lector(models.Model):
    name = models.CharField(verbose_name="ФИО Лектора", default='', blank=True, null=True, max_length=1024)
    description = RichTextField(verbose_name="Описание лектора", null=True, blank=True, default='', max_length=1024, config_name='main_ckeditor')
    photo = models.FileField(
        verbose_name="Фотография лектора", blank=True, null=True)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        verbose_name = 'лектор'
        verbose_name_plural = 'лекторы'
