from django.db import models
from ckeditor.fields import RichTextField


class Page(models.Model):
    name = models.CharField(verbose_name="Название страницы", max_length=512)
    created_date = models.DateTimeField(
        auto_now_add=True, verbose_name="Дата создания товара")
    slug = models.CharField(
        verbose_name="Человекопонятный путь", max_length=128, null=True, blank=True, unique=True)
    title = models.CharField(
        verbose_name="Титр страницы в браузере", max_length=128, null=True, blank=True)
    text = RichTextField(
        verbose_name="Содержимое страницы", max_length=20480)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        verbose_name = 'страница'
        verbose_name_plural = 'страницы'       
    