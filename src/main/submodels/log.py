from django.db import models
from src.users.models import User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from datetime import datetime
from django.conf import settings
from src.main.submodels.ware import PromoCode
from adminsortable.models import SortableMixin


class Log(SortableMixin, models.Model):
    datetime = models.DateTimeField(
        auto_now_add=True, verbose_name="Дата и время выставления счета")
    user = models.ForeignKey(
        User, verbose_name="пользователь", on_delete=models.PROTECT)
    content_type = models.ForeignKey(
        ContentType, on_delete=models.PROTECT, null=True, blank=True)
    object_id = models.PositiveIntegerField(null=True, blank=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    price = models.IntegerField(
        verbose_name="Стоимость", null=True, blank=True)
    number = models.PositiveIntegerField(
        verbose_name="номер счета", null=True, blank=True)
    promo_code = models.ForeignKey(
        'PromoCode', verbose_name="промо-код", on_delete=models.PROTECT, null=True, blank=True)
    payment_url = models.CharField(
        verbose_name="URL для оплаты счета", null=True, blank=True, max_length=200)
    paid_dt = models.DateTimeField(
        verbose_name="Дата и время оплаты счета", null=True, default=None, blank=True)

    def __str__(self):
        return f"{self.user}"

    class Meta:
        verbose_name = 'запись журнала'
        verbose_name_plural = 'записи журнала'        
        ordering = ['id',]

    def save(self, *args, **kwargs):
        if not self.pk:
            super(Log, self).save(*args, **kwargs)
            self.number = settings.LOG_NUMBER_DELTA + self.pk
            self.save(update_fields=['number'])
        else:
            super(Log, self).save(*args, **kwargs)
