from django.db import models
from .auth_cert import AuthCert
from ckeditor.fields import RichTextField


class Course(models.Model):
    name = models.CharField(verbose_name="Название",
                            default='', blank=True, null=True, max_length=128)
    short_description = models.TextField(
        verbose_name="Короткое описание", null=True, blank=True, default='', max_length=1024)
    full_description = RichTextField(
        verbose_name="Полное описание", null=True, blank=True, default='', max_length=20048, config_name='main_ckeditor')
    auth_cert = models.ForeignKey(AuthCert, verbose_name="удостоверение курса", on_delete=models.DO_NOTHING, null=True, blank=True)
    hidden = models.BooleanField(verbose_name="Скрыть", default=True)
    
    class Meta:
        verbose_name = "курс"
        verbose_name_plural = "курсы"