from django.db import models
from .ware import *


class PromoCode(models.Model):
    slug = models.CharField(verbose_name="Код",
                            max_length=512, unique=True)
    price = models.IntegerField(
        verbose_name="Стоимость по коду", null=True, blank=True)
    ware = models.ForeignKey('Ware', verbose_name="Товар", on_delete=models.DO_NOTHING, null=True, blank=True)

    class Meta:
        verbose_name = 'промо код'
        verbose_name_plural = 'промо коды'
