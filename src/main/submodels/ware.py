from django.db import models
from sort_order_field import SortOrderField
from ckeditor.fields import RichTextField
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.conf import settings
from django.utils import timezone
from datetime import timedelta
import requests
from django.shortcuts import get_object_or_404
from src.main.submodels.log import *
from src.main.submodels.promo_code import *


W_TYPE_WB = "WBNR"  # Вебинар
W_TYPE_VC = "VIDC"  # Видео
W_TYPE_EX = "EXAM"  # Экзамен
W_TYPE_GM = "GAME"  # Игра
W_TYPE_QQ = "WTQQ"  # Q - что такое - непонятно, взято из предыдущей версии

W_TYPE = (
    (W_TYPE_WB, "Вебинар"),
    (W_TYPE_VC, "Видео"),
    (W_TYPE_EX, "Экзамен"),
    (W_TYPE_GM, "Игра"),
    (W_TYPE_QQ, "Q"),
)


class Ware(models.Model):
    name = models.CharField(verbose_name="Название",
                            default='', blank=True, null=True, max_length=1024)
    number = models.CharField(verbose_name="Номер",
                              default='', blank=True, null=True, max_length=50)
    created_date = models.DateTimeField(
        auto_now_add=True, verbose_name="Дата создания товара")
    start_date = models.DateTimeField(
        verbose_name="Дата и время начала", blank=True, null=True)
    end_date = models.DateTimeField(
        verbose_name="Дата и время окончания", blank=True, null=True)
    hours = models.PositiveIntegerField(
        verbose_name="продолжительность (часов)", default=2)
    category = models.ManyToManyField(
        'Category', verbose_name="Категория", blank=True, related_name='wares')
    course = models.ForeignKey('Course', verbose_name="Курс", blank=True,
                               related_name='wares', on_delete=models.DO_NOTHING, null=True)
    edu_program = models.ManyToManyField(
        'EduProgram', verbose_name="Список образовательных программ", blank=True, related_name='wares')
    lector = models.ForeignKey('Lector', verbose_name="Лектор", null=True,
                               blank=True, on_delete=models.DO_NOTHING, related_name='wares')
    cert = models.ForeignKey('Cert', verbose_name="Сертификат", null=True,
                             blank=True, on_delete=models.DO_NOTHING, related_name='wares')
    type = models.CharField(choices=W_TYPE, default=W_TYPE_WB,
                            null=True, verbose_name="Тип Товара", max_length=4)
    short_list_description = RichTextField(
        verbose_name="Краткое описание в списке", null=True, blank=True, default='', max_length=1024, config_name='main_ckeditor')  # wareDesPre
    full_list_description = models.TextField(
        verbose_name="Полное описание в списке", null=True, blank=True, default='', max_length=2048)  # wareDesPost
    time_description = models.CharField(
        verbose_name="Описание времени", null=True, blank=True, max_length=1024)  # wareDesTime
    full_description = models.TextField(
        verbose_name="Полное описание", null=True, blank=True, default='', max_length=20480)  # wareDesPost
    price = models.IntegerField(
        verbose_name="Стоимость", null=True, blank=True)
    sort_order = SortOrderField(("Порядок"))
    hidden = models.BooleanField(verbose_name="Скрыть", default=True)
    logs = GenericRelation('Log', related_query_name='ware')

    def __str__(self):
        return f"{self.name}"

    @property
    def cert_is_ready(self):
        if self.cert:
            return self.end_date + timedelta(hours=settings.CERT_DOWNLOAD_DELAY) < timezone.now()
        return None

    @property
    def cert_ready_date(self):
        if self.cert:
            return self.end_date + timedelta(hours=settings.CERT_DOWNLOAD_DELAY)
        return None

    class Meta:
        verbose_name = 'продукт'
        verbose_name_plural = 'продукты'
