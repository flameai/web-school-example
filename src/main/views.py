from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from .serializers import *
from .models import *
from rest_framework import viewsets
from rest_framework.renderers import JSONRenderer
from django.db.models import Q
from django.utils import timezone
from django.core.mail import send_mail
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.template.loader import render_to_string
from rest_framework.decorators import api_view, renderer_classes, permission_classes, parser_classes, authentication_classes, action
from rest_framework.parsers import MultiPartParser, JSONParser
from rest_framework.renderers import TemplateHTMLRenderer, StaticHTMLRenderer
from rest_framework.views import APIView
import sys
from src.users.models import *
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from .permissions import IsMyObject
import requests
from rest_framework import mixins, status
from django.http import Http404
from src.main.models import *
from django.shortcuts import get_object_or_404
import urllib
import json
from django.http import FileResponse
from django.contrib.contenttypes.models import ContentType
from rest_framework.pagination import PageNumberPagination
from django.db.models import Sum, Max
import hashlib
from .order_creators import WareOrderCreator, AuthCertOrderCreator

class Mailer():
    @classmethod
    def send_mail_smtp(cls, recipient=None, subject=None, html_message=None):
        if not subject:
            subject = 'Спасибо за письмо'
        message = ''
        from_email = settings.EMAIL_HOST_USER
        if not recipient:
            recipient = 'andryukov@gmail.com'
        try:
            send_mail(subject=subject, message=message, from_email=from_email, recipient_list=[
                recipient.email, ], html_message=html_message)
        except:
            raise

    @classmethod
    def send_restore_mail(cls, recipient, request, redirect=None):
        redirectstr = f"&redirect={redirect}" if redirect else ""
        link = f"{request.build_absolute_uri().replace(request.get_full_path(),'')}?link={TempLink.objects.create(user=recipient).value}{redirectstr}"
        subject = "Ссылка восстановления пароля на сервере web-pedagogi.ru"
        html_message = render_to_string("mail/restore.html", {
            'first_name': recipient.first_name,
            'last_name': recipient.last_name,
            'middle_name': recipient.middle_name,
            'organization': recipient.organization,
            'link': link,
            'minutes': int(settings.LINKS_EXP_TIME/60)
        })
        try:
            cls.send_mail_smtp(recipient=recipient,
                               subject=subject, html_message=html_message)
            return True
        except:
            return False

    @classmethod
    def send_create_mail(cls, recipient, request, redirect=None):
        redirectstr = f"&redirect={redirect}" if redirect else ""
        link = f"{request.build_absolute_uri().replace(request.get_full_path(),'')}?link={TempLink.objects.create(user=recipient).value}{redirectstr}"
        subject = "Вы успешно зарегистрировались на платформе web-pedagogi.ru"
        html_message = render_to_string("mail/create.html", {
            'link': link,
            'minutes': int(settings.LINKS_EXP_TIME/60)
        })
        try:
            cls.send_mail_smtp(recipient=recipient,
                               subject=subject, html_message=html_message)
            return True
        except:
            return False


# Рендер фронта
def front_render_view(request, exception=None):
    return render(request, 'index.html')


class WareViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Набор АПИ для Товаров
    """
    renderer_classes = [JSONRenderer, ]
    permission_classes = [AllowAny, ]
    serializer_class = WareSerializer

    def get_queryset(self):
        if 'pk' in self.kwargs:
            return get_object_or_404(Ware, hidden=False, pk=int(self.kwargs['pk']))
        return Ware.objects.filter(hidden=False).order_by('start_date')

    def list(self, request):
        return Response(self.serializer_class(self.get_queryset(), many=True, context={'request': None}).data)

    def retrieve(self, request, pk):
        return Response(self.serializer_class(self.get_queryset(), context={'request': None}).data)

    @action(detail=False)
    def expecting(self, request, *args, **kwargs):
        qs = self.get_queryset().filter(
            start_date__gt=timezone.now())
        if 'category' in request.GET:
            categories = list(map(int, request.GET.getlist('category')))
            qs = qs.filter(category__pk__in=categories)
        if 'edu_program' in request.GET:
            edu_progs = list(map(int, request.GET.getlist('edu_program')))
            qs = qs.filter(edu_program__pk__in=edu_progs)
        return Response(self.serializer_class(qs.distinct(), many=True, context={'request': None}).data)

    @action(detail=True, methods=['post', ], permission_classes=[IsAuthenticated, ], authentication_classes=[JSONWebTokenAuthentication, ])
    def get_order_url(self, request, pk, *args, **kwargs):
        # Отправка заказа в эквайринг
        ware = get_object_or_404(Ware, pk=pk)
        order_url = WareOrderCreator().get_order_data(obj=ware, user=request.user, promo_code=request.data['promoCode'])
        return Response({"order_url": order_url}, status.HTTP_200_OK)


class AuthCertViewSet(viewsets.GenericViewSet):
    """
    Набор АПИ для удостоверений
    """
    renderer_classes = [JSONRenderer, ]
    permission_classes = [AllowAny, ]
    serializer_class = WareSerializer    

    @action(detail=True, methods=['post', ], permission_classes=[IsAuthenticated, ], authentication_classes=[JSONWebTokenAuthentication, ])
    def get_order_url(self, request, pk, *args, **kwargs):
        # Отправка заказа в эквайринг
        auth_cert = get_object_or_404(AuthCert, pk=pk)
        order_url = AuthCertOrderCreator().get_order_data(obj=auth_cert, user=request.user, title=f"Услуга почтовой отправки удостоверения {auth_cert.name}")
        return Response({"order_url": order_url}, status.HTTP_200_OK)


class LectorViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Набор АПИ для Лекторов
    """
    renderer_classes = [JSONRenderer, ]
    permission_classes = [AllowAny, ]
    serializer_class = LectorSerializer

    def get_queryset(self):
        return Lector.objects.all()


class CategoryViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Набор АПИ для списка категорий
    """
    renderer_classes = [JSONRenderer, ]
    permission_classes = [AllowAny, ]
    serializer_class = CategorySerializer

    def get_queryset(self):
        if self.action == 'expecting':
            return Category.objects.filter(hidden=False, wares__start_date__gt=timezone.now()).distinct()
        return Category.objects.filter(hidden=False)

    @action(detail=False)
    def expecting(self, request, *args, **kwargs):
        qs = self.get_queryset()
        if 'edu_program' in request.GET:
            qs = qs.filter(wares__edu_program__pk=int(request.GET['edu_program']), wares__end_date__gt=timezone.now(), wares__hidden=False)
        return Response(self.serializer_class(qs, many=True).data)


class ProjectViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Набор АПИ для проектов
    """
    permission_classes = (AllowAny,)
    serializer_class = ProjectSerializer
    pagination_class = None

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request'] = None
        return context

    def get_queryset(self):
        return Project.objects.filter(hidden=False)


class BannerViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Набор АПИ для баннеров
    """
    permission_classes = (AllowAny,)
    serializer_class = BannerSerializer
    pagination_class = None

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request'] = None
        return context

    def get_queryset(self):
        return Banner.objects.filter(hidden=False)


class LogViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
    """
    Набор АПИ для получения журнала покупок и сертификатов
    """
    serializer_class = LogSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    pagination_class = None

    def get_queryset(self):
        return Log.objects.all()

    def list(self, request):
        filter = Q(user=request.user, paid_dt__isnull=False)
        qs = self.get_queryset().filter(filter)
        return Response(LogSerializer(qs, many=True).data)

    @action(detail=False, methods=['get'])
    def wares(self, request):
        # Получить все оплаты продуктов с пагинацией
        filter = Q(user=request.user, paid_dt__isnull=False,
                   content_type=ContentType.objects.get_for_model(Ware))
        qs = self.get_queryset().filter(filter).order_by('-datetime')        
        return Response(LogSerializer(qs, many=True).data)

    @action(detail=False, methods=['get'])
    def auth_certs(self, request):
        # Получить детализацию по удостоверениям

        # Сертификаты по моим программам с посещенными часами
        visited_hours = Sum('edu_program__wares__hours', filter=Q(
            edu_program__wares__end_date__lt=timezone.now(), edu_program__wares__logs__paid_dt__isnull=False))
        user_auth_certs = AuthCert.objects.filter(edu_program__wares__logs__user__pk=request.user.pk, edu_program__wares__logs__paid_dt__isnull=False, edu_program__wares__end_date__lt=timezone.now(
        )).annotate(visited_hours=visited_hours).annotate(paid_date=Max('logs__paid_dt')).order_by('edu_program', '-hours')

        return Response(AuthCertSerializer(user_auth_certs, many=True).data)

    # @action(detail=False, methods=['get'])
    # def programs(self, request):
    #     # Получить детализацию по пройденным программам

    #     # Программы по моим программам с посещенными часами
    #     visited_hours = Sum('wares__hours', filter=Q(
    #         wares__end_date__lt=timezone.now(), wares__logs__paid_dt__isnull=False, hours__isnull=False))
    #     programs = EduProgram.objects.filter(wares__logs__user__pk=request.user.pk, wares__logs__paid_dt__isnull=False, 
    #     wares__end_date__lt=timezone.now()).annotate(visited_hours=visited_hours).order_by('name', '-hours')

    #     return Response(EduProgramSerializer(programs, many=True).data)


    @action(detail=True, methods=['get'], permission_classes=[IsMyObject])
    def cert(self, request, pk):
        obj = self.get_object()
        ware_obj = obj.content_object

        # Прошло достаточно времени для скачивания сертификата
        if not ware_obj.cert_is_ready:
            return Response({"error": "Сертификат еще не готов "}, status=status.HTTP_400_BAD_REQUEST)

        text_fio = f"{request.user.first_name} {request.user.middle_name} {request.user.last_name}"
        text_date = ware_obj.end_date.date().strftime('%d.%m.%Y')
        text_title = ware_obj.name
        result_file = f"{settings.CERT_IMAGES_FOLDER}{obj.pk}.jpg"
        ware_obj.cert.insert_text(
            text_fio=text_fio, text_date=text_date, text_title=text_title, result_file=result_file)

        return FileResponse(open(result_file, 'rb'))


class PageViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Набор АПИ для получения страниц
    """
    permission_classes = [AllowAny]
    serializer_class = PageSerializer

    def get_queryset(self):
        return Page.objects.all()
    lookup_field = 'slug'


class AdjustViewSet(viewsets.GenericViewSet, mixins.RetrieveModelMixin):
    """
    Набор АПИ для получения настроек
    """
    permission_classes = [AllowAny]
    serializer_class = AdjustSerializer

    def get_queryset(self):
        return Adjust.objects.all()
    lookup_field = 'slug'


class PromoCodeViewSet(viewsets.GenericViewSet, mixins.RetrieveModelMixin):
    """
    Набор АПИ для получения промокодов
    """
    permission_classes = [AllowAny]
    serializer_class = PromoCodeSerializer
    authentication_classes = [JSONWebTokenAuthentication]

    def get_queryset(self):
        return PromoCode.objects.all()

    def retrieve(self, request, slug):
        if not 'ware' in request.GET:
            return Response("", status.HTTP_400_BAD_REQUEST)
        ware_pk = int(request.GET['ware'])
        return self.get_code_for_ware_by_slug(slug=self.kwargs['slug'], ware_pk=ware_pk)

    lookup_field = 'slug'

    def get_code_for_ware_by_slug(self, slug, ware_pk):
        qs = PromoCode.objects.filter(
            Q(ware__pk=ware_pk) | Q(ware__pk__isnull=True))
        promo_code = get_object_or_404(qs, slug=slug)
        return Response(self.serializer_class(promo_code).data)


class EduProgramViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Набор АПИ для списка образовательных программ
    """
    renderer_classes = [JSONRenderer, ]
    permission_classes = [AllowAny, ]
    serializer_class = EduProgramSerializer
    pagination_class = None

    def get_queryset(self):
        return EduProgram.objects.all()


@api_view(['GET', ])
@renderer_classes([StaticHTMLRenderer])
@parser_classes([])
@authentication_classes([])
@permission_classes([])
def payment(request):
    """
    АПИ для платежной системы для получения сигнала об успешном платеже
    """
    sel = request.GET.get('SELLERID', '')
    ord = request.GET.get('ORDERID', '')
    sub = urllib.parse.parse_qs(request.META['QUERY_STRING'], encoding='cp1251')[
        'SUBJECT'][0]
    nam = request.GET.get('NAME', '')
    ema = request.GET.get('EMAIL', '')
    ip = request.GET.get('IP', '')
    refu = request.GET.get('REFERER_URL', '')
    ren = request.GET.get('REFERENCE_NO', '')
    res = request.GET.get('RESPONSE_CODE', '')
    mes = request.GET.get('MESSAGE', '')
    pay = request.GET.get('PAYED_BY', '')
    tot = request.GET.get('TOTAL', '')
    cur = request.GET.get('CURRENCY', '')
    com_r = request.GET.get('COMMISSION_RATE', '')
    com = request.GET.get('COMMISSION', '')
    dis = request.GET.get('DISCOUNT', '')
    tes = request.GET.get('TEST_MODE', '')
    con_id = request.GET.get('CONTRACT_ID', '')
    con = request.GET.get('CONTRACT', '')
    acc = request.GET.get('ACCOUNT', '')

    sig = request.GET['SIGNATURE']

    pwd = settings.PAY_ONLINE_PASS

    str = f"{sel}{ord}{sub}{nam}{ema}{ip}{refu}{ren}{res}{mes}{pay}{tot}{cur}{com_r}{com}{dis}{tes}{con_id}{con}{acc}{pwd}"

    if not sig.upper() == hashlib.md5(str.encode('cp1251')).hexdigest().upper():
        return Response("NO", status=status.HTTP_400_BAD_REQUEST)
    else:
        log = get_object_or_404(Log, pk=int(ord)-settings.LOG_NUMBER_DELTA)
        log.paid_dt = timezone.now()
        log.save(update_fields=['paid_dt'])
        return Response("YES", status=status.HTTP_200_OK)
