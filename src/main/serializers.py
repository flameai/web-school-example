from rest_framework.serializers import ModelSerializer, HyperlinkedModelSerializer, SerializerMethodField, RelatedField, ReadOnlyField, IntegerField, SerializerMethodField, DateTimeField
from .models import *

class CategorySerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'

class EduProgramSerializer(ModelSerializer):
    # visited_hours = SerializerMethodField()
    class Meta:
        model = EduProgram
        fields = '__all__'

    # def get_visited_hours(self, obj):        
    #     if not hasattr(obj,'visited_hours'):
    #         return None
    #     if not obj.hours:
    #         return None
    #     return obj.hours if obj.visited_hours > obj.hours else obj.visited_hours


class LectorSerializer(HyperlinkedModelSerializer):
    class Meta:
        model = Lector
        fields = '__all__'


class PageSerializer(HyperlinkedModelSerializer):
    class Meta:
        model = Page
        fields = '__all__'
        lookup_field = 'slug'
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }

class AdjustSerializer(ModelSerializer):
    class Meta:
        model = Adjust
        fields = '__all__'        


class WareSerializer(ModelSerializer):        
    lector = LectorSerializer()
    category = CategorySerializer(many=True)
    edu_program = EduProgramSerializer(many=True)

    class Meta:
        model = Ware
        fields = ['id', 'name', 'number', 'created_date', 'start_date', 'end_date',
                  'hidden', 'hours', 'category', 'course', 'edu_program', 'lector', 'type', 
                  'short_list_description', 'full_list_description', 'time_description', 'full_description', 
                  'price', 'sort_order', 'cert_is_ready', 'cert_ready_date']        


class ProjectSerializer(HyperlinkedModelSerializer):
    class Meta:
        model = Project
        fields = '__all__'


class BannerSerializer(HyperlinkedModelSerializer):
    class Meta:
        model = Banner
        fields = '__all__'

class AuthCertSerializer(ModelSerializer):
    edu_program = EduProgramSerializer(many=False)    
    is_ready_to_issue = SerializerMethodField()
    visited_hours = SerializerMethodField()
    paid_date = DateTimeField(read_only=True)
    class Meta:
        model = AuthCert
        fields = ['id', 'name', 'edu_program','visited_hours', 'hours', 'is_ready_to_issue', 'price', 'paid_date']

    def get_is_ready_to_issue(self, obj):
        # Готов к выдаче
        if not hasattr(obj,'visited_hours'):
            return None
        if not obj.hours:
            return None
        return obj.visited_hours >= obj.hours
    
    def get_visited_hours(self, obj):        
        if not hasattr(obj,'visited_hours'):
            return None
        if not obj.hours:
            return None
        return obj.hours if obj.visited_hours > obj.hours else obj.visited_hours

class LoggedObjectRelatedField(RelatedField):
    def to_representation(self, value):
        if isinstance(value, Ware):
            serializer = WareSerializer(value, context={"request": None})
        elif isinstance(value, AuthCert):
            serializer = AuthCertSerializer(value)
        else:
            raise Exception('Неожиданный тип ссылки')

        return serializer.data


class LogSerializer(ModelSerializer):
    entity = LoggedObjectRelatedField(source='content_object', read_only=True)

    class Meta:
        model = Log
        fields = '__all__'


class PromoCodeSerializer(ModelSerializer):
    class Meta:
        model = PromoCode
        fields = '__all__'
