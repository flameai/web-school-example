from django.conf import settings
from django.shortcuts import get_object_or_404
from src.main.models import Log, Ware, PromoCode
from django.contrib.contenttypes.models import ContentType
from django.utils import timezone
from datetime import timedelta
from abc import ABC, abstractmethod
import requests
import hashlib

###########################
# Классы-создатели счетов #
###########################


"""
Абстрактный класс-создатель счета для оплаты в системе эквайринга
"""
class OrderCreator(ABC):
    @abstractmethod
    def get_order_data(self):
        pass


"""
Общий класс-создатель счета для оплаты в системе эквайринга для всех сущностей
"""
class CommonOrderCreator(OrderCreator):
    def get_order_data(self):
        pass

"""
Класс-создатель счета для оплаты в системе эквайринга для Продукта
"""
class WareOrderCreator(CommonOrderCreator):
    def get_order_data(self, obj, user, promo_code=None, title=""):
        # Формирование данных для запроса заказа в эквайринге
        if not obj.pk:
            return None
        if not user:
            return None
        if promo_code:
            promo_code = get_object_or_404(PromoCode, slug=promo_code)
            price = promo_code.price
        else:
            price = obj.price

        log_order = Log.objects.create(user=user, content_type=ContentType.objects.get(
            app_label='main', model='ware'), object_id=obj.pk, price=price, promo_code=promo_code)

        sid = settings.PAY_ONLINE_SID
        ord = log_order.number
        acc = user.pk
        tot = price * 100
        cur = 'rub'
        em = user.email
        sub = title if title != "" else obj.name
        exp = (timezone.now() + timedelta(minutes=settings.PAY_ORDER_EXPIRATION)
            ).replace(microsecond=0).isoformat()
        pwd = settings.PAY_ONLINE_PASS
        str = f"{sid}{ord}{acc}{tot}{cur}{em}{sub}{exp}{pwd}"
        sig = hashlib.md5(str.encode()).hexdigest()
        url = "https://web-pedagogi.ru/?redirect=/pages/spasibo/"
        data = {
            "sid": sid,
            "ord": ord,
            "acc": acc,
            "tot": tot,
            "cur": cur,
            "em": em,
            "sub": sub,
            "exp": exp,
            "sig": sig,
            "url": url
        }
        response = requests.post(
            settings.PAY_ONLINE_ORDER_URL, data=data, verify=False)
        log_order.payment_url = response.url
        log_order.save(update_fields=['payment_url'])
        #### Отправка письма о том, что существует счет ####
        # Пока не написано  и не факт что понадобится
        ####################################################
        return response.url

"""
Класс-создатель счета для оплаты в системе эквайринга для Удостоверения
"""
class AuthCertOrderCreator(CommonOrderCreator):
    def get_order_data(self, obj, user, promo_code=None, title=""):
        # Формирование данных для запроса заказа в эквайринге
        if not obj.pk:
            return None
        if not user:
            return None
        if promo_code:
            promo_code = get_object_or_404(PromoCode, slug=promo_code)
            price = promo_code.price
        else:
            price = obj.price

        log_order = Log.objects.create(user=user, content_type=ContentType.objects.get(
            app_label='main', model='authcert'), object_id=obj.pk, price=price, promo_code=promo_code)

        sid = settings.PAY_ONLINE_SID
        ord = log_order.number
        acc = user.pk
        tot = price * 100
        cur = 'rub'
        em = user.email
        sub = title if title != "" else obj.name
        exp = (timezone.now() + timedelta(minutes=settings.PAY_ORDER_EXPIRATION)
            ).replace(microsecond=0).isoformat()
        pwd = settings.PAY_ONLINE_PASS
        str = f"{sid}{ord}{acc}{tot}{cur}{em}{sub}{exp}{pwd}"
        sig = hashlib.md5(str.encode()).hexdigest()
        url = "https://web-pedagogi.ru/?redirect=/pages/spasibo/"
        data = {
            "sid": sid,
            "ord": ord,
            "acc": acc,
            "tot": tot,
            "cur": cur,
            "em": em,
            "sub": sub,
            "exp": exp,
            "sig": sig,
            "url": url
        }
        response = requests.post(
            settings.PAY_ONLINE_ORDER_URL, data=data, verify=False)
        log_order.payment_url = response.url
        log_order.save(update_fields=['payment_url'])

        #### Отправка письма о том, что существует счет ####
        # Пока не написано  и не факт что понадобится
        ####################################################
        return response.url


### Устарело ###
def get_order_data(obj, user, promo_code=None, title=""):
    # Формирование данных для запроса заказа в эквайринге
    if not obj.pk:
        return None
    if not user:
        return None
    if promo_code:
        promo_code = get_object_or_404(PromoCode, slug=promo_code)
        price = promo_code.price
    else:
        price = obj.price

    log_order = Log.objects.create(user=user, content_type=ContentType.objects.get(
        app_label='main', model='ware'), object_id=obj.pk, price=price, promo_code=promo_code)

    sid = settings.PAY_ONLINE_SID
    ord = log_order.number
    acc = user.pk
    tot = price * 100
    cur = 'rub'
    em = user.email
    sub = title if title != "" else obj.name
    exp = (timezone.now() + timedelta(minutes=settings.PAY_ORDER_EXPIRATION)
           ).replace(microsecond=0).isoformat()
    pwd = settings.PAY_ONLINE_PASS
    str = f"{sid}{ord}{acc}{tot}{cur}{em}{sub}{exp}{pwd}"
    sig = hashlib.md5(str.encode()).hexdigest()
    url = "https://web-pedagogi.ru/?redirect=/pages/spasibo/"
    data = {
        "sid": sid,
        "ord": ord,
        "acc": acc,
        "tot": tot,
        "cur": cur,
        "em": em,
        "sub": sub,
        "exp": exp,
        "sig": sig,
        "url": url
    }
    response = requests.post(
        settings.PAY_ONLINE_ORDER_URL, data=data, verify=False)
    log_order.payment_url = response.url
    log_order.save(update_fields=['payment_url'])
    #### Отправка письма о том, что существует счет ####
    # Пока не написано  и не факт что понадобится
    ####################################################
    return response.url
