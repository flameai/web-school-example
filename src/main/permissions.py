from rest_framework.permissions import BasePermission
import hashlib
from django.conf import settings

class IsMyAccountGETparam(BasePermission):
    def has_permission(self, request, view):
        return int(request.GET['user']) == request.user.id

class IsMyObject(BasePermission):
    def has_permission(self, request, view):
        return view.get_object().user == request.user