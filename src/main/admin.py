from django.contrib import admin
from src.main.models import *
from ckeditor.widgets import CKEditorWidget
from django import forms
from django.http import HttpResponse
import base64
from django.shortcuts import render
import io
from django.conf import settings
from django.utils.safestring import mark_safe
from django.contrib.contenttypes.admin import GenericTabularInline
from adminsortable.admin import NonSortableParentAdmin, SortableTabularInline, SortableGenericTabularInline, SortableAdmin


# Register your models here.

class LectorAdminForm(forms.ModelForm):
    description = forms.CharField(widget=CKEditorWidget(config_name='main_ckeditor'), label="Описание")

    class Meta:
        model = Lector
        fields = '__all__'
        labels = {'description': 'Описание'}


class LectorAdmin(admin.ModelAdmin):
    form = LectorAdminForm
    search_fields = ['name', ]
    readonly_fields = ['id', ]


admin.site.register(Lector, LectorAdmin)


class WareCustomers(SortableGenericTabularInline):
    model = Log
    verbose_name = "покупатель"
    verbose_name_plural = "покупатели"

    def get_queryset(self, request):
        qs = super(WareCustomers, self).get_queryset(request)
        return qs.filter(paid_dt__isnull=False)
    extra = 0
    readonly_fields = ['datetime', 'paid_dt', 'user', 'promo_code', 'payment_url', 'price', 'number']


class WareAdminForm(forms.ModelForm):
    short_list_description = forms.CharField(widget=CKEditorWidget(config_name='main_ckeditor'), label="Описание")

    class Meta:
        model = Ware
        fields = '__all__'
        labels = {
            "short_list_description": "Текст"
        }


class WareAdmin(NonSortableParentAdmin):
    model = Ware
    form = WareAdminForm
    change_form_template = "admin/ware/form.html"
    list_display = ('name', 'visible', 'lector', 'start_date')
    search_fields = ['name', ]
    fields = ['name', 'start_date', 'end_date', 'category', 'course', 'edu_program',
              'lector', 'cert', 'short_list_description', 'time_description', 'price', 'hidden']
    autocomplete_fields = ['lector', 'cert', 'course']

    def response_change(self, request, obj):
        if "cert_preview" in request.POST and obj.cert:
            obj.save()
            data = {}
            image_wt = obj.cert.insert_text(text_title=obj.name)
            buffered = io.BytesIO()
            image_wt.save(buffered, format="JPEG")
            img_str = base64.b64encode(buffered.getvalue())
            data["image"] = bytes.decode(img_str, 'utf-8')
            return render(request, 'admin/cert/index.html', data)
        return super().response_change(request, obj)

    def visible(self, obj):
        return not obj.hidden

    visible.boolean = True
    visible.short_description = 'активно'
    visible.admin_order_field = 'hidden'

    inlines = [WareCustomers, ]


admin.site.register(Ware, WareAdmin)


class CategoryAdmin(admin.ModelAdmin):
    pass


admin.site.register(Category, CategoryAdmin)


class PageAdminForm(forms.ModelForm):
    text = forms.CharField(
        widget=CKEditorWidget(config_name='page'))

    class Meta:
        model = Page
        fields = '__all__'


class PageAdmin(admin.ModelAdmin):
    form = PageAdminForm
    list_display = ['created_date', 'name', 'title', 'url']
    readonly_fields = ['created_date', 'url']

    def url(self, obj):
        path = f"/{settings.PAGE_PREFFIX}/{obj.slug}/"
        return mark_safe(f"<a href='{path}'>{path}</a>")


admin.site.register(Page, PageAdmin)


class AdjustAdminForm(forms.ModelForm):
    text = forms.CharField(
        widget=CKEditorWidget(config_name='page'))

    class Meta:
        model = Page
        fields = '__all__'


class AdjustAdmin(admin.ModelAdmin):
    form = AdjustAdminForm
    list_display = ['name']


admin.site.register(Adjust, AdjustAdmin)


class CertAdmin(admin.ModelAdmin):
    search_fields = ['name']

    class Meta:
        model = Cert
    change_form_template = "admin/cert/form.html"

    def response_change(self, request, obj):
        if "test" in request.POST:
            obj.save()
            data = {}
            image_wt = obj.insert_text()
            buffered = io.BytesIO()
            image_wt.save(buffered, format="JPEG")
            img_str = base64.b64encode(buffered.getvalue())
            data["image"] = bytes.decode(img_str, 'utf-8')
            return render(request, 'admin/cert/index.html', data)
        return super().response_change(request, obj)


admin.site.register(Cert, CertAdmin)


class CourseAdmin(admin.ModelAdmin):
    search_fields = ['name', ]
    pass


admin.site.register(Course, CourseAdmin)


class AuthCertAdmin(admin.ModelAdmin):
    pass


admin.site.register(AuthCert, AuthCertAdmin)


class BannerAdmin(admin.ModelAdmin):
    list_display = ('name', 'visible', 'sort_order')

    def visible(self, obj):
        return not obj.hidden

    visible.boolean = True
    visible.short_description = 'активно'
    visible.admin_order_field = 'hidden'


admin.site.register(Banner, BannerAdmin)


class ProjectAdmin(admin.ModelAdmin):
    list_display = ('name', 'visible', 'sort_order')

    def visible(self, obj):
        return not obj.hidden

    visible.boolean = True
    visible.short_description = 'активно'
    visible.admin_order_field = 'hidden'


admin.site.register(Project, ProjectAdmin)


class LogAdmin(admin.ModelAdmin):
    list_display = ('datetime', 'number', 'user', 'content_object', 'paid_dt')
    readonly_fields = ['number', 'datetime']
    search_fields = ['number', ]


admin.site.register(Log, LogAdmin)


class PromoCodeAdmin(admin.ModelAdmin):
    list_display = ('slug', 'ware', 'price')
    autocomplete_fields = ['ware', ]


admin.site.register(PromoCode, PromoCodeAdmin)


class EduProgramAdmin(admin.ModelAdmin):
    pass


admin.site.register(EduProgram, EduProgramAdmin)
