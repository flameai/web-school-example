
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from src.main import views

router = DefaultRouter()
router.register(r'ware', views.WareViewSet, basename='ware')
router.register(r'lector', views.LectorViewSet, basename='lector')
router.register(r'category', views.CategoryViewSet, basename='category')
router.register(r'banner', views.BannerViewSet, basename='banner')
router.register(r'project', views.ProjectViewSet, basename='project')
router.register(r'log', views.LogViewSet, basename='log')
router.register(r'page', views.PageViewSet, basename='page')
router.register(r'promocode', views.PromoCodeViewSet, basename='promocode')
router.register(r'auth_cert', views.AuthCertViewSet, basename='auth_cert')
router.register(r'edu_program', views.EduProgramViewSet, basename='edu_program')
router.register(r'adjust', views.AdjustViewSet, basename='adjust')

urlpatterns = [
    path('', include(router.urls)),
    path('payment/', views.payment, name="payment")
]
